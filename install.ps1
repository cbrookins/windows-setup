<#
VERSION: 1.0.1
DESCRIPTION: My computer setups for desktop and racing sim.
#>
$ErrorActionPreference = 'SilentlyContinue'
#Requires -RunAsAdministrator
$type = $args[0] # 'sim' or <blank>

# WALLPAPER STYLE
$style = 'gradient' # gradient or stripe

# INSTALL WSL
wsl --install # also installs Ubuntu

# WINGET PACKAGES ALL
$packages = @(
    '7zip.7zip',
    'Discord.Discord',
    'Brave.Brave',
    'OBSProject.OBSStudio',
    'OBSProject.obs-amd-encoder',
    'Spotify.Spotify',
    'Valve.Steam',
    'VideoLAN.VLC'
)



# BUILD PACKAGE LIST BASED ON ARGUMENT
if ($type -eq 'sim') {
    # WINGET SIM ONLY
    $sim = @(
        'Logitech.GHUB'
    )
    
    foreach ($s in $sim) {
        $packages += $s
    }
} else {
    # WINGET DESKTOP PACKAGES
    $desktop = @(
        'Balena.Etcher',
        'CloneHeroTeam.CloneHero',
        'ElectronicArts.EADesktop',
        'Elgato.StreamDeck',
        'EpicGames.EpicGamesLauncher',
        'GIMP.GIMP',
        'Git.Git',
        'GOG.Galaxy',
        'HandBrake.HandBrake',
        'HandBrake.HandBrake.CLI',
        'MacroDeck.MacroDeck',
        'Microsoft.PowerShell',
        'Microsoft.VisualStudioCode',
        'MoritzBunkus.MKVToolNix',
        'Nvidia.GeForceNow',
        'OpenShot.OpenShot',
        'Rufus.Rufus',
        'Meltytech.Shotcut',
        'Ubisoft.Connect'##,
        ##'VB-Audio.Voicemeeter.Banana'
    )
    
    foreach ($d in $desktop) {
        $packages += $d
    }
}

# INSTALL WINGET PACKAGES
winget install $packages --silent

# RELOAD PATH ENV VARIABLE
$env:Path = [System.Environment]::GetEnvironmentVariable("Path","Machine") + ";" + [System.Environment]::GetEnvironmentVariable("Path","User") 

# SCHEDULE DAILYWALLPAPER
Set-Location  $env:USERPROFILE
git clone https://codeberg.org/cbrookins/daily-wallpapers.git
$trigger = New-ScheduledTaskTrigger -Daily -At 9:00am
$action = New-ScheduledTaskAction -Execute PowerShell -Argument "-NoProfile -ExecutionPolicy Bypass -File $env:USERPROFILE\daily-wallpapers\dailyWallpaper.ps1 $style" -WorkingDirectory "$env:USERPROFILE\daily-wallpapers"
Register-ScheduledTask 'Daily Wallpaper' -Action $action -Trigger $trigger

# SCHEDULE WINGET UPPDATES
$trigger = New-ScheduledTaskTrigger -Daily -At 7:00am
$action = New-ScheduledTaskAction -Execute cmd.exe -Argument "/C winget upgrade --all --silent"
Register-ScheduledTask 'Winget Updates' -Action $action -Trigger $trigger -RunLevel Highest

# RUN AND CLEANUP WIN11 DEBLOAT
Set-Location $env:USERPROFILE
git clone https://github.com/LeDragoX/Win-Debloat-Tools.git
Set-Location $env:USERPROFILE\Win-Debloat-Tools
Set-ExecutionPolicy Unrestricted -Scope CurrentUser -Force; ls -Recurse *.ps*1 | Unblock-File; .\"WinDebloatTools.ps1"

Set-Location $env:USERPROFILE
Remove-Item -Path $env:USERPROFILE\Win-Debloat-Tools -Recurse -Force -Confirm:$false